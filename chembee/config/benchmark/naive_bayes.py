from chembee.config.benchmark.BenchmarkAlgorithm import BenchmarkAlgorithm
from sklearn.naive_bayes import GaussianNB, MultinomialNB, BernoulliNB, ComplementNB
import os
import sys

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..")))


class NaiveBayesClassifier(BenchmarkAlgorithm):
    name = "naive-bayes"

    algorithms = (
        GaussianNB(),
        MultinomialNB(),
        BernoulliNB(),
        ComplementNB(),
    )

    titles = ("Gaussian", "Multinomial", "Bernoulli", "Complement")
