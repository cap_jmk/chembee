from chembee.config.benchmark.svc import SVClassifier
from chembee.config.benchmark.svc_poly import SVCPolyClassifier
from chembee.config.benchmark.spectral_clustering import SpectralClusteringClassifier
from chembee.config.benchmark.random_forest import RandomForestClassifier
from chembee.config.benchmark.naive_bayes import NaiveBayesClassifier
from chembee.config.benchmark.logistic_regression import LogisticRegressionClassifier
from chembee.config.benchmark.linear_regression import LinearRegressionClassifier
from chembee.config.benchmark.kmeans import KMeansClassifier
from chembee.config.benchmark.knn import KNNClassifier
from chembee.config.benchmark.mlp_classifier import NeuralNetworkClassifier
from chembee.config.benchmark.restricted_bm import RBMClassifier


algorithms = [
    SVClassifier,
    # SVCPolyClassifier,
    SpectralClusteringClassifier,
    RandomForestClassifier,
    NaiveBayesClassifier,
    LogisticRegressionClassifier,
    LinearRegressionClassifier,
    KMeansClassifier,
    KNNClassifier,
    NeuralNetworkClassifier,
    RBMClassifier,
]
