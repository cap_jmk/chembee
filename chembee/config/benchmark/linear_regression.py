from chembee.config.benchmark.BenchmarkAlgorithm import BenchmarkAlgorithm
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet
import os
import sys


sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..")))


class LinearRegressionClassifier(BenchmarkAlgorithm):

    name = "linear-regression"

    algorithms = (LinearRegression(), Ridge(), Lasso(), ElasticNet())
    titles = (
        "Ordinary Least Squares",
        "Ridge",
        "Lasso",
        "Elastic net",
    )
