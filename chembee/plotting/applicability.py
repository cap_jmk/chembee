import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def plot_applicability_domain(res: dict, interval: tuple, file_name):
    """
    The plot_applicability_domain function plots the number of times each applicability value was observed in the
    results. This is useful for determining if there are any obvious outliers or other issues with the data.

    :param result:dict: Used to Store the results of the classifier.
    :param file_name: Used to Specify the name of the file that will be saved.
    :return: A plot of the counts of each applicability value.

    :doc-author: Julian M. Kleber
    """
    plt.figure(figsize=(15, 15))
    matplotlib.rcParams.update({"font.size": 32})
    counts = []
    start, stop, step = interval
    for i, key in enumerate(res.keys()):
        count = len(res[key][1])
        counts.append(count)

    plt.scatter(np.arange(start, stop, step), counts,
                color="black", marker="x", s=180)
    plt.plot(np.arange(start, stop, step), counts, linewidth=2, color="black")
    plt.xlabel("Applicability")
    plt.ylabel("Counts")
    plt.savefig(file_name, dpi=300)
