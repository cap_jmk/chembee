import sys, os

 

from chembee.datasets.BreastCancer import BreastCancerDataset
from actions.evaluation import (
    screen_classifier_for_metrics,
    screen_classifier_for_metrics_stratified,
)
from chembee.plotting.evaluation import (
    plot_collection,
    plot_collection_stratified,
    plot_combined_bar_chart,
)

from chembee.config.calibration.random_forest import RandomForestClassifier
from chembee.config.calibration.knn import KNNClassifier
from chembee.config.calibration.mlp_classifier import NeuralNetworkClassifierRELU
from chembee.config.calibration.svc import NaivelyCalibratedSVCRBF

from tests.prepare_biodeg import *
from chembee.utils.file_utils import make_full_filename


def test_screen_classifier_for_metrics_cross_val(data_set, full_data):
    data_set = data_set
    X_data, y_true = full_data
    n = 5
    clf_list = [
        RandomForestClassifier,
        KNNClassifier,
        NeuralNetworkClassifierRELU,
        NaivelyCalibratedSVCRBF,
    ]
    metrics = screen_classifier_for_metrics_stratified(
        X_data, y_true, clf_list=clf_list, n=n
    )
    metric_types = ["scalar", "array", "matrix"]
    assert metric_types == list(metrics.keys())

    for metric in metric_types:
        assert len(metrics[metric]) == len(clf_list)
        for clf in clf_list:
            if metric == "scalar":
                assert len(metrics[metric][clf.name]) == 5
            elif metric == "array":
                assert len(metrics[metric][clf.name]) == 4

    plot_collection_stratified(
        metrics,
        file_name=data_set.name + "_average_evaluation",
        prefix="plots/evaluation",
    )
    file_name = make_full_filename(
        file_name=data_set.name + "grouped_average_evaluation.png",
        prefix="plots/evaluation",
    )
    plot_combined_bar_chart(metrics["scalar"], file_name=file_name)


def test_screen_classifier_for_metrics_fitted(data_set):
    data_set = data_set
    clf_list = [
        RandomForestClassifier,
        KNNClassifier,
        NeuralNetworkClassifierRELU,
        NaivelyCalibratedSVCRBF,
    ]
    clf_result = []
    for clf in clf_list:
        clf.fit(data_set.X_train, data_set.y_train)
        clf_result.append(clf)
    metrics = screen_classifier_for_metrics(
        X_train=data_set.X_train.to_numpy(),
        X_test=data_set.X_test.to_numpy(),
        y_train=data_set.y_train.to_numpy().astype(np.int32),
        y_test=data_set.y_test.to_numpy().astype(np.int32),
        clf_list=clf_result,
        to_fit=False,
    )
    assert ["scalar", "array", "matrix"] == list(metrics.keys())


def test_screen_classifier_for_metrics_not_fitted(data_set):
    data_set = data_set
    clf_list = [
        RandomForestClassifier,
        KNNClassifier,
        NeuralNetworkClassifierRELU,
        NaivelyCalibratedSVCRBF,
    ]
    metrics = screen_classifier_for_metrics(
        X_train=data_set.X_train.to_numpy(),
        X_test=data_set.X_test.to_numpy(),
        y_train=data_set.y_train.to_numpy().astype(np.int32),
        y_test=data_set.y_test.to_numpy().astype(np.int32),
        clf_list=clf_list,
        to_fit=True,
    )
    assert ["scalar", "array", "matrix"] == list(
        metrics.keys()
    )  # scalar, array, and matrix are types of metrics that may be returned
    plot_collection(
        metrics, file_name=data_set.name + "_evaluation", prefix="plots/evaluation"
    )
