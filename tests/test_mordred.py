import sys, os

 

from mordred import Calculator, descriptors
from chembee.preparation.processing import calculate_mordred_descriptors
from chembee.datasets.BioDegDataSet import BioDegDataSet

from chembee.plotting.lipinski import polar_plot
from chembee.preparation.processing import calculate_lipinski_desc


def test_calculate_mordred_descriptors():
    """
    Tests the realiability of the Mordred calculator
    """

    calc = Calculator(descriptors, ignore_3D=False)
    len(calc.descriptors)

    file_name = "test"
    prefix = "tests/plots"
    target = "ReadyBiodegradability"
    DataSet = BioDegDataSet(
        split_ratio=0.7, data_set_path="tests/data/Biodeg.sdf", target=target
    )
    data = calculate_mordred_descriptors(list(DataSet.mols)[:2])
    assert len(data.columns.to_list()) == len(calc.descriptors), (
        "The length of the calculated features"
        + str(data.columns.to_list())
        + "do not match the amount of descriptor "
        + str(len(calc.descriptors))
    )
    assert not data.isnull().values.any(), "Mordred calculation returned NaN values "


def test_polar_plot():
    """
    The test_polar_plot function loads a data file and plots it in polar coordinates.

    The test_polar_plot function loads the Biodeg.sdf dataset from the data directory,
    and then creates a polar plot of each molecule's HBA/HBD ratio on a polar coordinate system.
    This is accomplished by using matplotlib's PolarAxes to create the plot, which is then saved as an image file in the output directory.

    :param script_loc: Used to Specify the location of the script to be tested.
    :return: A list of the dataframe and a figure.

    :doc-author: Trelent
    """

    file_name = "test"
    prefix = "tests/plots"
    target = "ReadyBiodegradability"
    DataSet = BioDegDataSet(
        split_ratio=0.7, data_set_path="tests/data/Biodeg.sdf", target=target
    )
    DataSet.data = calculate_lipinski_desc(DataSet.data.head(100), DataSet.mols)
    polar_plot(DataSet, file_name=file_name, prefix=prefix)
