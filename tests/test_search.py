import sys, os
import numpy as np

 

from actions.search import (
    screen_fingerprints_against_data,
    get_similar_compounds,
)
from chembee.preparation.processing import (
    convert_mols_to_morgan_fp,
    convert_mols_to_rdk_fp,
)
from tests.prepare_biodeg import *
from chembee.preparation.processing import get_mols_from_supplier


def test_fingerprints_for_bitvectors(data_set):
    """
    The test_fingerprints_for_bitvectors function tests the convert_mols_to_morgan_fp and convert_mols
    to rdk fp functions. It takes in a data set, which is a list of molecules, and returns morgan fingerprints
    and rdk fingerprints for each molecule in the data set. The function tests that both types of fingerprint
    returned are bitvectors.

    :param data_set: Used to pass in the data set that is being used for the test.

    :doc-author: Julian M. Kleber
    """

    data_set = data_set
    indices = [0, 1]
    mols = get_mols_from_supplier(indices=indices, supplier=data_set.mols)
    mfps = convert_mols_to_morgan_fp(mols, radius=3, n_bits=2048, return_bit=True)
    rfps = convert_mols_to_rdk_fp(mols, return_bit=True)
    assert type(mfps) == type(rfps) == type([])
    assert len(mfps[0]) == 2048


def test_morgan_search(data_set, fitted_clf, false_pred, base_mols):
    """
    The test_morgan_search function takes in a data set, a fitted classifier, and two lists of false positive
    and negative molecules. It then converts the false positive and negative molecules to Morgan fingerprints
    and screens them against the base_mols (the whole database). The function returns a list of all the
    false positives that are not screened out by the classifier.

    :param data_set: Used to specify the data set to be used.
    :param fitted_clf: Used to fit the classifier to the data set.
    :param false_pred: Used to store the false positive and negative molecules.
    :param base_mols: Used to generate a list of all the molecules in the training set.
    :return: A list of the false positive and negative molecules.

    :doc-author: Julian M. Kleber
    """

    false_pos_mols, false_neg_mols = false_pred
    fp_pos = convert_mols_to_morgan_fp(false_pos_mols, radius=3, n_bits=2048)
    fp_neg = convert_mols_to_morgan_fp(false_neg_mols, radius=3, n_bits=2048)
    fp_base = convert_mols_to_morgan_fp(base_mols, radius=3, n_bits=2048)
    result_mfp = screen_fingerprints_against_data(fp_pos, fp_base)
    result_mfn = screen_fingerprints_against_data(fp_neg, fp_base)


def test_rdk_fps(data_set, fitted_clf, false_pred, base_mols):
    """
    The test_rdk_fps function takes in a data set, a fitted classifier, and the false
    predictions from that classifier. It then takes the false positive molecules and
    the base molecules (molecules used to train the model) and converts them into RDKit
    fingerprints. The fingerprints are then screened against each other using an inner join.
    The function returns two lists of RDK fingerprints: one for all of the false positives
    that were not screened out by this inner join, and one for all of those that were.

    :param data_set: Used to retrieve the data that is used to train the classifier.
    :param fitted_clf: Used to fit the model to the data.
    :param false_pred: Used to store the molecules that are predicted to be false positives and false negatives.
    :param base_mols: Used to calculate the false positive rate.
    :return: A list of the false positive molecules that are present in the training set.

    :doc-author: Julian M. Kleber
    """

    false_pos_mols, false_neg_mols = false_pred
    fp_pos = convert_mols_to_rdk_fp(false_pos_mols)
    fp_neg = convert_mols_to_rdk_fp(false_pos_mols)
    fp_base = convert_mols_to_rdk_fp(base_mols)
    result_rdk_fp = screen_fingerprints_against_data(fp_pos, fp_base)
    result_rdk_fn = screen_fingerprints_against_data(fp_neg, fp_base)


def test_ada_search(full_data, data_set, false_pred_indices):
    """
    The test_ada_search function tests the get_similar_compounds function. It takes in a full dataset,
    a data set of interest (the false positives or false negatives), and the indices of those compounds
    in the original data set. The test then checks that a dictionary is returned with keys as compound names
    and values as lists containing tuples with two elements: 1) another compound name and 2) a similarity score.

    :param full_data: Used to get the data for which we want to find similar compounds.
    :param data_set: Used to get the correct data from the full_data.
    :param false_pred_indices: Used to get the indices of the false positives and negatives.
    :return: A dictionary.

    :doc-author: Julian M. Kleber
    """

    X_data, y_data = full_data
    false_pos, false_neg = false_pred_indices
    try:
        result, similarities_table = get_similar_compounds(
            compounds_of_interest=X_data[np.array(false_pos)], compounds=X_data
        )
    except Exception as e:
        print(e)
    finally:
        print("Moving on")
    try:
        result, similarities_table = get_similar_compounds(
            compounds_of_interest=X_data[np.array(false_pos)], compounds=X_data
        )
    except Exception as e:
        print(e)
    finally:
        print("Moving on")
    assert type(result) == type({})
    assert type(similarities_table) == type({})
